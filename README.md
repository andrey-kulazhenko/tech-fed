## [Devias Kit - Admin Main](https://react-material-dashboard.devias.io/) [![Tweet](https://img.shields.io/twitter/url/http/shields.io.svg?style=social&logo=twitter)](https://twitter.com/intent/tweet?text=%F0%9F%9A%A8Devias%20Freebie%20Alert%20-%20An%20awesome%20ready-to-use%20register%20page%20made%20with%20%23material%20%23react%0D%0Ahttps%3A%2F%2Fdevias.io%20%23createreactapp%20%23devias%20%23material%20%23freebie%20%40devias-io)

> Free React Main made with [Material UI's](https://material-ui.com/?ref=devias-io) components, [React](https://reactjs.org/?ref=devias-io) and of course [create-react-app](https://facebook.github.io/create-react-app/?ref=devias-io) to boost your app development process! We'll launch a pro version soon, so if you are interested subscribe to our personal emailing list on [https://devias.io/](https://devias.io/)

## Quick start

- Make sure your NodeJS and npm versions are up to date for `React 16.8.6`

- Install dependencies: `npm install` or `yarn`

- Start the server: `npm run start` or `yarn start`

- Views are on: `localhost:3000`

## Documentation

The documentation for the React Material Kit is can be found [here](https://material-ui.com?ref=devias-io).

## Exercise details

The following are the details of the requirements of the exercise as it should be performed by the FED:

1. Build a web-app/mobile-app using reactjs/react-native and typescript

1. The app is a TikTok clone it should allow the basic functionality which is
    1. Web: App should use the https://react-material-dashboard.devias.io/app/dashboard package components if\where applicable. Pages should be responsive.
        1. Main page
            1. Web: Page is made of the following components
                1. Top header - menu with two options (main and discover), current time in hours and minutes
                1. First side (50% of the page), list of names of all liked videos (empty to begin with)
                1. Second side (50% of the page), videos view (same as in mobile)
                1. Bottom footer (Showing only Sorbet Exercise @current-year)
            1. Mobile: user should be able to scroll/slide up/down across videos from other users (but not her\his own), each video should show the user name who uploaded it and video title, scroll should be an “infinite” scroll.
            1. For the sake of the exercise feel free to pull videos from any source and put some random/fake title and user name
            1. The user should be able to like\unlike a video by clicking it (likes will be saved locally and shown on the videos that were liked)
        1. Discover page, first row (30% of the screen size) is a slider of 2 images, second row is trending videos, only show list of videos with scroll to the right to get more (more videos should be called only during the scroll, lazy loading), third row videos loaded in the last hour. Clicking a video will play it.
            1. Web: page should be in the same header\footer context
        1. Mobile only: Swipe left for the camera and take a video of up to 10 seconds, eace video should get a title
            1. For the sake of the exercise just save it on the device
