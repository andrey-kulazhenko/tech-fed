import React from 'react';
import { Navigate } from 'react-router-dom';
import { PartialRouteObject } from 'react-router';
import { MainLayout } from './layouts/MainLayout';
import { Discover } from './views/Discover';
import { Main } from './views/Main';
import { NotFoundView } from './views/Errors';

const routes: PartialRouteObject[] = [
  {
    path: '/',
    element: <MainLayout />,
    children: [
      { path: 'main', element: <Main /> },
      { path: 'discover', element: <Discover /> },
      { path: '404', element: <NotFoundView /> },
      { path: '/', element: <Navigate to="/main" /> },
      { path: '*', element: <Navigate to="/404" /> }
    ]
  }
];

export default routes;
