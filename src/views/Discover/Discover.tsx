import React from 'react';
import Page from '../../components/Page';
import { Slider } from './Slider';
import { TrendingVideos } from './TrendingVideos';
import { Container, Grid, makeStyles } from '@material-ui/core';
import { RecentVideos } from './RecentVideos';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.default,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  container: {
    '& > *': {
      marginBottom: 10
    }
  },
  slider: {
    height: 'calc(100vh * 0.3)',
  }
}));

export const Discover = (): JSX.Element => {
  const classes = useStyles();

  return (
    <Page
      className={classes.root}
      title="Account"
    >
      <Container maxWidth="md">
        <Grid
            container
            spacing={3}
            direction='column'
            className={classes.container}
        >
          <Grid xs={12} className={classes.slider} item>
            <Slider />
          </Grid>
          <Grid xs={12} item>
            <TrendingVideos />
          </Grid>
          <Grid xs={12} item>
            <RecentVideos />
          </Grid>
        </Grid>
      </Container>
    </Page>
  );
};
