import React, { useState } from 'react';
import clsx from 'clsx';
import {
    Avatar,
    Card,
    CardContent,
    CardHeader,
    Divider,
    makeStyles,
    Typography
} from '@material-ui/core';
import moment from 'moment';
import { take } from 'lodash';
import ReactPlayer from 'react-player/youtube';
import { videos } from '../Main/constants';
import { Skeleton } from '@material-ui/lab';

const useStyles = makeStyles(() => ({
    root: {
        width: '100%'
    },
    container: {
        padding: 10,
        overflowX: 'scroll',
        whiteSpace: 'nowrap'
    },
    card: {
        display: 'inline-block',
        width: '35%',
        marginRight: 10
    },
    description: {
        '& p': {
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            textOverflow: 'ellipsis'
        }
    },
    media: {
        height: 250,
        width: 'auto',
        maxWidth: 500,
        marginLeft: 15
    }
}));

interface TrendingVideosProps {
    className?: string;
}

export const TrendingVideos = ({ className }: TrendingVideosProps): JSX.Element => {
    const classes = useStyles();
    const [limit, setLimit] = useState(3);
    const [loading, setLoading] = useState(false);

    const handleScroll = (e: React.UIEvent<HTMLElement>) => {
        if (videos.length <= limit || loading) {
            return;
        }
        const scrollWidth = e.currentTarget.scrollWidth;
        const scrollLeft = e.currentTarget.scrollLeft;
        const clientWidth = e.currentTarget.clientWidth;
        if (scrollWidth === scrollLeft + clientWidth) {
            setLoading(true);
            setTimeout(() => {
                setLimit(limit + 1);
                setLoading(false);
            }, 2000);
        }
    };

    return (<Card className={clsx(classes.root, className)}>
        <CardHeader title='Trending Videos' />
        <Divider />
        <div className={classes.container} onScroll={handleScroll}>
            {take(videos, limit).map(video => (
                <Card className={classes.card} key={video.id}>
                    <CardHeader
                        avatar={<Avatar alt={video.user.name} src={video.user.avatar} />}
                        title={video.user.name}
                        subheader={moment(video.timestamp).fromNow()}
                    />
                    <div className={classes.media}>
                        <ReactPlayer url={video.url} height="inherit" width="inherit" />
                    </div>
                    <CardContent className={classes.description}>
                        <Typography variant='body2' color='textSecondary' component='p'>
                            {video.name}
                        </Typography>
                    </CardContent>
                </Card>
            ))}
            {loading && <Loader />}
        </div>
    </Card>);
};

const Loader = () => {
    const classes = useStyles();
    return (<Card className={classes.card} style={{ width: '33%' }}>
        <CardHeader
            avatar={<Skeleton animation='wave' variant='circle' width={40} height={40} />}
            title={<Skeleton animation='wave' height={10} width='80%' style={{ marginBottom: 6 }} />}
            subheader={<Skeleton animation='wave' height={10} width='40%' />}
        />
        <Skeleton animation='wave' variant='rect' className={classes.media} />

        <CardContent className={classes.description}>
            <Skeleton animation='wave' height={10} />
        </CardContent>
    </Card>);
};
