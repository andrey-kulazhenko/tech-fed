import React from 'react';
import clsx from 'clsx';
import {
    Avatar,
    Card, CardContent,
    CardHeader,
    Divider, makeStyles, Typography
} from '@material-ui/core';
import moment from 'moment';
import ReactPlayer from 'react-player/youtube';
import { videos } from '../Main/constants';

const useStyles = makeStyles(() => ({
    root: {
        width: '100%'
    },
    container: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        padding: 10
    },
    card: {
        width: '30%'
    },
    media: {
        height: 250,
        width: 'auto',
        maxWidth: 500,
        marginLeft: 15
    },
}));

interface RecentVideosProps {
    className?: string;
}

export const RecentVideos = ({className}: RecentVideosProps): JSX.Element => {
    const classes = useStyles();
    const recentVideos = videos.filter(v => moment(v.timestamp).isSameOrAfter(moment().subtract(1, 'hour')));

    return (<Card className={clsx(classes.root, className)}>
        <CardHeader title="Recent Videos"/>
        <Divider/>
        <div className={classes.container}>
            {recentVideos.map(rv => (
                <Card className={classes.card} key={rv.id}>
                    <CardHeader
                        avatar={<Avatar alt={rv.user.name} src={rv.user.avatar}/>}
                        title={rv.user.name}
                        subheader={moment(rv.timestamp).fromNow()}
                    />
                    <div className={classes.media}>
                        <ReactPlayer url={rv.url} height="inherit" width="inherit" />
                    </div>
                    <CardContent>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {rv.name}
                        </Typography>
                    </CardContent>
                </Card>
            ))}
        </div>
    </Card>);
};
