import React from 'react';
import clsx from 'clsx';
import {
    Card,
    makeStyles
} from '@material-ui/core';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { Carousel } from 'react-responsive-carousel';

const useStyles = makeStyles(() => ({
    root: {
        height: '100%',
    },
    slider: {
        height: '100%',
        width: 'auto',
        '& .carousel-slider': {
            height: '100%',
            width: 'auto',
        }
    }
}));

interface SliderProps {
    className?: string;
}

export const Slider = ({className, ...rest}: SliderProps): JSX.Element => {
    const classes = useStyles();

    return (
        <Card
            className={clsx(classes.root, className)}
            {...rest}
        >
            <Carousel infiniteLoop dynamicHeight className={classes.slider} showThumbs={false}>
                <div>
                    <img src="/static/images/slider/landscape-1.jpg" alt="slide 1" />
                </div>
                <div>
                    <img src="/static/images/slider/landscape-2.jpg" alt="slide 2" />
                </div>
                <div>
                    <img src="/static/images/slider/landscape-3.jpg" alt="slide 3" />
                </div>
            </Carousel>
        </Card>
    );
};
