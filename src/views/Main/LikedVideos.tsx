import React from 'react';
import clsx from 'clsx';
import {
  Card,
  CardHeader,
  Divider,
  List,
  ListItem,
  ListItemText,
  makeStyles
} from '@material-ui/core';
import { Video } from './constants';

const useStyles = makeStyles(({
  root: {
    height: '100%'
  },
  image: {
    height: 48,
    width: 48
  }
}));

interface LikedVideosProps {
  className?: string;
  videos: Video[];
}

export const LikedVideos = ({ className, videos, ...rest }: LikedVideosProps): JSX.Element => {
  const classes = useStyles();

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <CardHeader
        subtitle={`${videos.length} has been liked`}
        title="Liked Videos"
      />
      <Divider />
      <List>
        {videos.map((video, i) => (
          <ListItem divider={i < videos.length - 1} key={video.id}>
            <ListItemText primary={video.name} />
          </ListItem>
        ))}
      </List>
    </Card>
  );
};
