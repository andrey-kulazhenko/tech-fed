import React, {useState} from 'react';
import { Container, Grid, makeStyles } from '@material-ui/core';
import Page from '../../components/Page';
import { Videos } from './Videos';
import { LikedVideos } from './LikedVideos';
import { videos } from './constants';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.default,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

export const Main = (): JSX.Element => {
  const classes = useStyles();
  const [data, setData] = useState(videos);

  const likedVideos = data.filter(video => video.liked);

  return (
    <Page
      className={classes.root}
      title="Main"
    >
      <Container maxWidth={false}>
        <Grid
          container
          spacing={3}
        >
          <Grid item lg={6} xs={12}>
            <LikedVideos videos={likedVideos} />
          </Grid>
          <Grid item lg={6} xs={12}>
            <Videos videos={data} updateVideos={setData} />
          </Grid>
        </Grid>
      </Container>
    </Page>
  );
};
