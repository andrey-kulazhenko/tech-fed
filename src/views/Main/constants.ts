import { v4 as uuid } from 'uuid';
import moment from 'moment';

export interface Video {
    id: string;
    name: string;
    url: string;
    user: {
        name: string;
        avatar: string;
    };
    timestamp: string;
    liked: boolean;
}

export const videos: Video[] = [
    {
        id: uuid(),
        name: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
        url: 'https://www.youtube.com/watch?v=YE7VzlLtp-4',
        user: {
            name: 'Joni Lord',
            avatar: '/static/images/avatars/avatar_1.png'
        },
        timestamp: moment().subtract(4, 'minutes').format('YYYY-MM-DD HH:mm'),
        liked: false
    },
    {
        id: uuid(),
        name: 'Fusce interdum purus massa, nec dapibus lacus dictum id',
        url: 'https://www.youtube.com/watch?v=WjoDEQqyTig',
        user: {
            name: 'Sian Lin',
            avatar: '/static/images/avatars/avatar_2.png'
        },
        timestamp: moment().subtract(24, 'minutes').format('YYYY-MM-DD HH:mm'),
        liked: false
    },
    {
        id: uuid(),
        name: 'Donec purus sem, elementum id tincidunt ut, vestibulum in eros',
        url: 'https://www.youtube.com/watch?v=ba62uuv-5Dc',
        user: {
            name: 'Dawn Chester',
            avatar: '/static/images/avatars/avatar_3.png'
        },
        timestamp: moment().subtract(5, 'days').format('YYYY-MM-DD HH:mm'),
        liked: false
    },
    {
        id: uuid(),
        name: 'Maecenas velit enim, pulvinar id fermentum eu, tempor sit amet nulla',
        url: 'https://www.youtube.com/watch?v=JMJXvsCLu6s',
        user: {
            name: 'Clay William',
            avatar: '/static/images/avatars/avatar_4.png'
        },
        timestamp: moment().subtract(15, 'days').format('YYYY-MM-DD HH:mm'),
        liked: false
    },
    {
        id: uuid(),
        name: 'Praesent nec dolor tellus. Sed placerat turpis quis augue pretium, vel lacinia quam hendrerit',
        url: 'https://www.youtube.com/watch?v=iTWuZav-elY',
        user: {
            name: 'Bogdan Clements',
            avatar: '/static/images/avatars/avatar_5.png'
        },
        timestamp: moment().subtract(1, 'month').format('YYYY-MM-DD HH:mm'),
        liked: false
    },
    {
        id: uuid(),
        name: 'Nullam dapibus venenatis lectus, a congue enim efficitur sit amet',
        url: 'https://www.youtube.com/watch?v=3wey4jnzU74',
        user: {
            name: 'Scarlette Steadman',
            avatar: '/static/images/avatars/avatar_6.png'
        },
        timestamp: moment().subtract(3, 'months').format('YYYY-MM-DD HH:mm'),
        liked: false
    },
];
