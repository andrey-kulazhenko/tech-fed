import React, { useState } from 'react';
import clsx from 'clsx';
import { useBottomScrollListener } from 'react-bottom-scroll-listener';
import {
    Avatar,
    Card, CardActions,
    CardContent,
    CardHeader,
    Divider, IconButton,
    makeStyles,
    Typography
} from '@material-ui/core';
import { take } from 'lodash';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { Skeleton } from '@material-ui/lab';
import moment from 'moment';
import ReactPlayer from 'react-player/youtube';
import { Video } from './constants';

const useStyles = makeStyles((theme) => ({
    root: {},
    card: {
        marginBottom: 10
    },
    media: {
        height: 250,
        width: 'auto',
        maxWidth: 500,
        margin: '0 15px',
        [theme.breakpoints.down('md')]: {
            width: 'calc(100% - 20px)',
            height: '35vw',
            maxWidth: 'none',
            margin: '0 15px'
        }
    },
    scrollable: {
        overflow: 'auto',
        height: 'calc(100vh - 205px)'
    },
    liked: {
        color: 'red'
    }
}));

interface VideosProps {
    className?: string;
    videos: Video[];
    // eslint-disable-next-line no-unused-vars
    updateVideos: (videos: Video[]) => void;
}

export const Videos = ({className, videos, updateVideos, ...rest}: VideosProps): JSX.Element => {
    const classes = useStyles();
    const [loading, setLoading] = useState(false);
    const [limit, setLimit] = useState(3);

    const infiniteScroll = () => {
        if (limit >= videos.length || loading) return;
        setLoading(true);
        setTimeout(() => {
            setLimit(limit + 1);
            setLoading(false);
        }, 2000);
    }

    const scrollRef = useBottomScrollListener<HTMLDivElement>(infiniteScroll);

    const likeVideo = (id: string) => {
        const index = videos.findIndex(v => v.id === id);
        const video = videos[index];
        const nextVideos = [...videos];
        nextVideos[index] = { ...video, liked: !video.liked };
        updateVideos(nextVideos);
    };

    return (
        <Card
            className={clsx(classes.root, className)}
            {...rest}
        >
            <CardHeader title="Videos"/>
            <Divider/>
            <div className={classes.scrollable} ref={scrollRef}>
                {take(videos, limit).map(video => (
                    <Card key={video.id} className={classes.card}>
                        <CardHeader
                            avatar={<Avatar alt={video.user.name} src={video.user.avatar}/>}
                            title={video.user.name}
                            subheader={moment(video.timestamp).fromNow()}
                        />
                        <div className={classes.media}>
                            <ReactPlayer url={video.url} height="inherit" width="inherit" />
                        </div>
                        <CardContent>
                            <Typography variant="body2" color="textSecondary" component="p">
                                {video.name}
                            </Typography>
                        </CardContent>
                        <CardActions disableSpacing>
                            <IconButton onClick={() => likeVideo(video.id)} className={video.liked ? classes.liked : ''}>
                                <FavoriteIcon/>
                            </IconButton>
                        </CardActions>
                    </Card>
                ))}
                {loading && <Loader />}
            </div>
        </Card>
    );
};

const Loader = () => {
    const classes = useStyles();
    return (<Card className={classes.root}>
        <CardHeader
            avatar={<Skeleton animation="wave" variant="circle" width={40} height={40}/>}
            title={<Skeleton animation="wave" height={10} width="80%" style={{marginBottom: 6}}/>}
            subheader={<Skeleton animation="wave" height={10} width="40%"/>}
        />
        <Skeleton animation="wave" variant="rect" className={classes.media}/>
        <CardContent>
            <React.Fragment>
                <Skeleton animation="wave" height={10} style={{marginBottom: 6}}/>
                <Skeleton animation="wave" height={10} width="80%"/>
            </React.Fragment>
        </CardContent>
        <CardActions disableSpacing>
            <Skeleton animation="wave" variant="circle" width={40} height={40}/>
        </CardActions>
    </Card>);
};
