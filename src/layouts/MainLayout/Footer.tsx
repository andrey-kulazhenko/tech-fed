import React from 'react';
import { makeStyles } from '@material-ui/core';
import moment from 'moment';

const useStyles = makeStyles((theme) => ({
    footer: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.background.default,
        width: '100%',
        textAlign: 'center',
        padding: '10px 0',
        position: 'absolute',
        bottom: 0
    }
}));

export const Footer = (): JSX.Element => {
    const classes = useStyles();
    return (
        <footer className={classes.footer}>
            {moment().format('YYYY')}
        </footer>
    );
};
