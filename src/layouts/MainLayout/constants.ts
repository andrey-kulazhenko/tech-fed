import { Home as MainIcon, Icon, Video as VideoIcon } from 'react-feather';

export interface NavItems {
    href: string;
    icon: Icon;
    title: string;
}

export interface UserProps {
    avatar: string;
    jobTitle: string;
    name: string;
}

export const items: NavItems[] = [
    {
        href: '/main',
        icon: MainIcon,
        title: 'Main'
    },
    {
        href: '/discover',
        icon: VideoIcon,
        title: 'Discover'
    },
];

export const user: UserProps = {
    avatar: '/static/images/avatars/avatar_3.png',
    jobTitle: 'Senior FullStack Developer',
    name: 'Andrey K'
};
