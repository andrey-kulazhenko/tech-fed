import React, { useState } from 'react';
import moment from 'moment';
import { makeStyles, Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        fontSize: '1.2rem',
        [theme.breakpoints.down('md')]: {
            fontSize: '1rem'
        }
    }
}));

export const CurrentTime = (): JSX.Element => {
    const classes = useStyles();
    const [currentDate, updateCurrentDate] = useState(moment());

    setInterval(() => updateCurrentDate(moment()), 1000);

    return (
        <Typography className={classes.root}>{currentDate.format('HH:mm')}</Typography>
    );
};
