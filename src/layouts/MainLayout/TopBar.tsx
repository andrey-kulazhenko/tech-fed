import React, { MouseEventHandler } from 'react';
import clsx from 'clsx';
import { AppBar, Box, Hidden, IconButton, Toolbar, makeStyles, Link } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import { CurrentTime } from './CurrentTime';
import { items } from './constants';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {},
  avatar: {
    width: 60,
    height: 60
  },
  menuItem: {
    fontSize: '1.2rem',
    marginRight: 20,
    color: theme.palette.background.default,
    fontWeight: 'bold'
  }
}));

interface TopBarProps {
  className?: string,
  onMobileNavOpen: MouseEventHandler
}

export const TopBar = ({
  className,
  onMobileNavOpen,
  ...rest
}: TopBarProps): JSX.Element => {
  const classes = useStyles();

  return (
    <AppBar
      className={clsx(classes.root, className)}
      elevation={0}
      {...rest}
    >
      <Toolbar>
        <Hidden mdDown>
          <Typography>
            {items.map((item) => <Link key={item.title} className={classes.menuItem} href={item.href}>{item.title}</Link>)}
          </Typography>
        </Hidden>
        <Hidden lgUp>
          <CurrentTime />
        </Hidden>
        <Box flexGrow={1} />
        <Hidden mdDown>
          <CurrentTime />
        </Hidden>
        <Hidden lgUp>
          <IconButton
            color="inherit"
            onClick={onMobileNavOpen}
          >
            <MenuIcon />
          </IconButton>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
};
