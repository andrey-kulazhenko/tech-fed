import React, { forwardRef, ReactElement } from 'react';
import { Helmet } from 'react-helmet';

interface PageProps extends React.ComponentPropsWithoutRef<'div'> {
  children: ReactElement;
  title: string;
}

const Page = forwardRef<HTMLInputElement, PageProps>(({
  children,
  title = '',
  ...rest
}, ref) => {
  return (
    <div
      ref={ref}
      {...rest}
    >
      <Helmet>
        <title>{title}</title>
      </Helmet>
      {children}
    </div>
  );
});

Page.displayName = 'Page';

export default Page;
